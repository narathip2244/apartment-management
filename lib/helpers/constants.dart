import 'package:flutter/material.dart';

class Const {
  static const space24 = 24.0;
  static const space20 = 20.0;
  static const space16 = 16.0;
  static const space12 = 12.0;
  static const space8 = 8.0;
}

/// Size
double baseTextSize = 16; // clamp(16, 18) from main
double baseIconSize = 20; // clamp(20, 30)
const double basePadding = 10; // default
const double baseBorderRadius = 5; // default

// Color
Color APP_PRIMARY_COLOR = const Color.fromARGB(255, 0, 11, 107);

// ignore: constant_identifier_names
const List<Locale> SUPPORTED_LOCALES = [
  Locale('en'),
  Locale('th'),
];

class Illustrations {
  static const onBoarding1 = 'assets/images/onboarding1.png';
  static const onBoarding2 = 'assets/images/onboarding2.png';
  static const onBoarding3 = 'assets/images/onboarding3.png';
  static const forgotPassword =
      'assets/images/illustration_forgot_password.png';
  static const emptyRating = 'assets/images/empty_rating.png';
  static const emptyCart = 'assets/images/empty_cart.png';
  static const checkoutSuccess = 'assets/images/checkout_success.png';
  static const favoriteIllustration = 'assets/images/favorite_illustration.png';
  static const emptyOrder = 'assets/images/empty_order.png';
  static const emptySearch = 'assets/images/empty_search.png';
}

class Images {
  static const logoLight = 'assets/images/logo_light.png';
  static const logoDark = 'assets/images/logo_dark.png';
  static const logoFacebook = 'assets/images/logo_facebook.png';
  static const logoGoogle = 'assets/images/logo_google.png';
  static const done = 'assets/images/done.png';
  static const dress = 'assets/images/dress.png';
  static const highHeel = 'assets/images/high_heel.png';
  static const menuIcon = 'assets/images/menu_icon.png';
  static const womanBag = 'assets/images/woman_bag.png';
  static const doneBadge = 'assets/images/done_badge.png';
  static const arrived = 'assets/images/arrived.png';
  static const package = 'assets/images/package.png';
  static const payment = 'assets/images/payment.png';
  static const rating = 'assets/images/rating.png';
  static const travelling = 'assets/images/travelling.png';
  static const cod = 'assets/images/cod.png';
  static const creditCard = 'assets/images/credit_card.png';
  static const paypal = 'assets/images/paypal.png';
  static const automotive = 'assets/images/automotive.png';
  static const computer = 'assets/images/computer.png';
  static const electronic = 'assets/images/electronic.png';
  static const shirt = 'assets/images/shirt.png';
  static const shoes = 'assets/images/shoes.png';
  static const smartphone = 'assets/images/smartphone.png';
  static const watches = 'assets/images/watches.png';
}

class Routes {
  static const String splash = '/splash';
  static const String address = '/address';
  static const String browseProduct = '/browse_product';
  static const String cart = '/cart';
  static const String category = '/category';
  static const String checkout = '/checkout';
  static const String checkoutSuccess = '/checkout_success';
  static const String editProfile = '/edit_profile';
  static const String favorite = '/favorite';
  static const String forgotPassword = '/forgot_password';
  static const String home = '/home';
  static const String language = '/language';
  static const String notification = '/notification';
  static const String onBoarding = '/on_boarding';
  static const String orderHistory = '/order_history';
  static const String orderDetail = '/order_detail';
  static const String payment = '/payment';
  static const String paymentSuccess = '/payment_success';
  static const String product = '/product';
  static const String profile = '/profile';
  static const String signIn = '/sign_in';
  static const String signUp = '/sign_up';
}
