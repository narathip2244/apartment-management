run: 
	flutter run

clean:
	flutter clean && flutter pub get
clean-ios-pod:
	flutter clean && flutter pub get
	cd ios && pod install

clean-mac-pod:
	flutter clean && flutter pub get
	cd macos && pod install

ios-pod:
	cd ios && pod install

mac-pod:
	cd macos && pod install

build-mac:
	flutter build macos

aab:
	flutter build appbundle

apk:
	flutter build apk

win:
	flutter build windows

re-gradlew:
	cd android && .\gradlew clean

release:
	flutter run --release

db-open:
	npx json-server --no-cors --host 172.16.69.203 db.json

db-reopen:
	npx kill-port 3000
	npx json-server --no-cors --host 172.16.69.203 db.json

db-watch:
	json-server --watch db.json

db-rewatch:
	npx kill-port 3000
	json-server --watch db.json

db-ppid:
	lsof -t -i:3000 -sTCP:LISTEN

db-kill:
	npx kill-port 3000

uninstall :
	adb -s {deviceId} uninstall com.confidia.howToPayPOS

UNa :
	adb -s 73610ef00121 uninstall com.confidia.howToPayPOS

test-apis :
	flutter test test\apis

re-Icon :
	flutter pub run flutter_launcher_icons:main
	
win-copy :
	cd build\windows\x64\runner\Release && ren htp_pos_mobile.exe HowToPayPOS.exe
	cd build\windows\x64\runner && ren Release HowToPayPOS && powershell Compress-Archive HowToPayPOS howtopay-window.zip 
	cd build\windows\x64\runner && scp howtopay-window.zip syd-dev-app5.howtopay.com:/home/na/

make-copy-to-server :
	scp "D:\howtopay-window.zip" syd-dev-app5.howtopay.com:/home/na/
